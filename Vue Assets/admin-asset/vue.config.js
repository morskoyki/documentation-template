module.exports = {
  devServer: {
    proxy: {
      "/panel/*": {
        target: process.env.VUE_APP_ADMIN_API,
        secure: false
      },
      "/media/*": {
        target: process.env.VUE_APP_API,
        secure: false
      },
      "/api/*": {
        target: process.env.VUE_APP_API,
        secure: false
      }
    },
    disableHostCheck: true
  }
};
