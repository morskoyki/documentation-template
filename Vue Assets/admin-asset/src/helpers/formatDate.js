export const formatDate = function (date) {
  let dd = date.getDate();
  if (dd < 10) dd = '0' + dd;

  let mm = date.getMonth() + 1; // месяц 1-12
  if (mm < 10) mm = '0' + mm;

  let yyyy = date.getFullYear();
  while (yyyy.length < 4) yyyy.unshift('0');

  return dd + '.' + mm + '.' + yyyy;
}

export const formatDateAndTime = function (date, isUTC = false) {
  let hh = isUTC ? date.getUTCHours() : date.getHours();
  if (hh < 10) hh = '0' + hh;

  let mm = isUTC ? date.getUTCMinutes() : date.getMinutes();
  if (mm < 10) mm = '0' + mm;

  let ss = isUTC ? date.getUTCSeconds() : date.getSeconds();
  if (ss < 10) ss = '0' + ss;
  return formatDate(date, isUTC) + ' ' + hh + ':' + mm + ':' + ss;
}
