export default function auth({
  next,
  store
}) {
  if (!store.state.login.isLoggedIn && !localStorage.getItem('authToken')) {
    return next({
      name: 'Login'
    })
  }
  return next()
}
