import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

import Login from "@/pages/Login";
import Home from "@/pages/Home";

Vue.use(VueRouter)

const routes = [{
  path: '/',
  name: 'Home',
  component: Home,
  redirect: "/users/list",
  meta: {
    layout: "main",
  }
},
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      layout: "empty",
    }
  },
  {
    path: '/users',
    name: 'Настройка доступов',
    component: () => import('@/pages/Users'),
    redirect: "/users/list",
    meta: {
      layout: "main",
    },
    children: [{
      path: "/users/list",
      name: "Настройка доступов",
      meta: {
        layout: "main",
      },
      component: () => import(`@/pages/Users/List`)
    },
      {
        path: "/users/add",
        name: "Добавить пользователя",
        meta: {
          layout: "main",
        },
        component: () => import(`@/pages/Users/Add`)
      },
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  routes,
  linkExactActiveClass: "nav-item active",
  linkActiveClass: "nav-item active"
})

router.beforeEach((to, from, next) => {
  if (!to.meta.middleware) {
    return next()
  }

  const middleware = to.meta.middleware

  const context = {
    to,
    from,
    next,
    store,
  }
  return middleware[0]({
    ...context
  })
})

export default router
