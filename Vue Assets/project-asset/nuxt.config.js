require("dotenv").config();
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'VueBox',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  // Конфиг модуля авторизации
  auth: {
    localStorage: false,
    cookie: {
      prefix: 'auth.',
      options: {
        path: '/',
        expires: 7 // days
      }
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "",
            method: "post",
            propertyName: "api_token"
          },
          user: {
            url: "",
            method: "get",
            propertyName: false
          },
          logout: {
            url: "",
            method: "get",
            propertyName: false
          }
        }
      }
    },
    redirect: {
      login: '/',
      logout: '/',
      callback: '/',
      home: false,
    },
    plugins: [{
      src: '~/plugins/errorHandler',
      ssr: true
    }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [{
    src: '~/assets/scss/main.scss',
    lang: 'scss'
  }, ],

  axios: {
    proxy: true,
    baseURL: process.env.API_URL,
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "@/plugins/errorHandler", // Отправка на layout error при неудачном реквесте
  ],

  proxy: {
    "/api": {
      target: process.env.API_URL,
    },
  },

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/style-resources',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'bootstrap-vue/nuxt',
    "@nuxtjs/axios",
    '@nuxtjs/proxy',
    '@nuxtjs/dotenv',
  ],

  styleResources: {
    scss: ["~assets/scss/common/_variables.scss",
      "~assets/scss/common/_mixins.scss"
    ]
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: null,
  }
}
