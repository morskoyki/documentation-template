export default (context) => {
  context.$axios.onError((err) => {
    if (err.response.status == '401') {
      context.$auth.logout();
    } else if (
      String(err.response.status)[0] == '5' ||
      err.response.status == '404'
    ) {
      context.error({
        statusCode: err.response.status,
        message: err.response.data?.message || err,
      });
    }
    throw err;
  })
}
