// Кастомная директива для Vue, которая позволяет определить клик снаружи элемента и обработать это событие (например закрытия модального окна при клике вне него)

import Vue from 'vue';

export const ClickOutside = {
  bind (el, binding, vNode) {
    if (typeof binding.value !== 'function') {
      const componentName = vNode.context.name;
      let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function`;
      if (componentName) {
        warn += `Found in component '${componentName}'`;
      }
      console.warn(warn);
    }
    const bubble = binding.modifiers.bubble;
    const handler = (e) => {
      if (bubble || (!el.contains(e.target) && el !== e.target)) {
        binding.value(e);
      }
    };
    el.__vueClickOutside__ = handler;
    document.addEventListener('click', handler);
  },

  unbind (el) {
    document.removeEventListener('click', el.__vueClickOutside__);
    el.__vueClickOutside__ = null;
  }
};

Vue.directive('click-outside', ClickOutside);
