# Содержание
\[\[_TOC_\]\]

> Каждый нижеперечисленный пункт может быть переопределен на уровне проекта или даже страницы. Если вы не согласны с каким-то из пунктов - обсудите это с тимлидом.


# Рабочая среда
Рекомендованный редактор кода: `Visual Studio Code`.

## Расширения для VScode
Рекомендованные расширения:

- Vetur
- Prettier
- GitLens
- ESLint

Дополнительные расширения:

- Better Comments
- Quokka.js
- TODO Tree

# Подключаемые фреймворки
- bootstrap-vue

# Общие требования к верстке

## Требования к коду
Верстка должна быть блочной, семантической, кроссбраузерной (требования к браузерам перечислены выше), должна соответствовать стандартам языков HTML5 и CSS3. Все файлы выполняются в кодировке UTF-8 (без BOM).
Приветствуется подробное комментирование кода в HTML, CSS и JS.
Обязательны комментарии хотя бы по основным структурным элементам.

### Общие

1. Входные параметры для компонента должны быть определены как можно более подробно. Описывайте тип props и значение по умолчанию.
2. Не используйте `snake_case` и `kebab-case` в вашем коде.
3. Называйте функции с использование глаголов (`check`,`set`). Если функция Предикат (возвращает булево) именуйте ее через `is...`.
4. Стили подключаются инлайном в компоненте только тогда. когда они относятся только к компоненту.
5. глобальные стили лежат в ассетах и мы их подключаем через nuxt.

### HTML
- Для разметки используются теги, актуальные для стандарта HTML5;
- Глобальные стили CSS и JS-скрипты должны быть вынесены в отдельные файлы;
- Значения всех атрибутов заключаются в двойные кавычки (");
- Форматирование HTML иерархическое;
- Код должен проходить валидацию W3C https://validator.w3.org/, кроме оправданных исключений;
- Поля форм должны иметь корректный тип (text, tel, email, search и т.д.), если заранее известно их содержание;
- Если семантика элемента недостаточна, необходимо использовать WAI-ARIA разметку;
- Обозначить при помощи разметки scheme.org следующие области страницы – «хлебные крошки», сетки товаров/услуг в каталогах, контактные данные, карточки товаров/услуг, отзывы.

### CSS
- Препроцессор **SCSS**, это строгое требование. Вы **не можете** использовать SASS синтаксис
- Составление CSS стилей должно подчиняться определенной методологии, чтобы в дальнейшем можно было масштабировать и поддерживать код. Можно выбрать любую общепринятую – БЭМ, SMACSS, OOCSS, MCSS и т.д, либо использовать продуманную свою;
- Стилевые свойства необходимо привязывать к классам (кроме оправданных исключений). Привязка стилевых свойств к id запрещена.
- Названия классов должны быть говорящими и определяются по их функциональному назначению или визуальному отображению. **Запрещается** писать классы транслитом;
- Каждое свойство в CSS — на отдельной строке;
- Каждый селектор в CSS — на отдельной строке;
- Указать цвет фона для body, даже если он белый;
- Небольшие декоративные изображения должны быть вставлены в CSS при помощи data:URL, либо оформлены в спрайт. В случае растровых изображений должны присутствовать альтернативные варианты графики двойного разрешения (для Retina мониторов);
- Каскад стилей должен быть сведен к минимуму;
- В стилевом файле в свойстве font-family у тега body должны быть прописаны альтернативные шрифты для всех распространенных операционных систем. Шрифты в @font-face должны быть прописаны каждый в 3х вариантах – woff2, woff и ttf;
- Если необходима нормализация нативных стилей браузера, нужно использовать normalize.css. CSS Reset (полный сброс CSS -свойств) использовать не желательно, ибо в дальнейшем это плохо сказывается на качестве верстки.
- Предпочтительные использование составных селекторов в оправданных случаях, например, margin: 10px 0 20px, а не margin-top, -left, -right, -bottom.
- Используемые `include mixins` — через строковую отбивку от основных стилей;
- Все примиси и переменные должны быть вынесены в отдельные файлы, и подключены в config файле (например, в nuxt.config.js) для глобальной видимости
- Для стилизации компонентов использовать инлайновые стили, а не импорт стилей `(@import “@/../component-style.scss”)` в теге `<style>` внутри компонента. Обязательно указать язык `(lang="scss/sass")`. Если нужно инкапсулировать стили, то использовать атрибут scoped. Глобальные и локальные стили можно сочетать.

### JS
- Все, что можно сделать без Javascript, делать без него;
- Все классы отвечающие за инициализацию скриптов должны иметь префикс js-;
- Если в верстке будут использованы фреймворки, необходимо заранее позаботится об их совместимости друг с другом;
- Код js-скриптов, написанных исполнителем, должен быть валидирован одним из инструментов на выбор -  JSLint / JSHint / ESLint. Готовые плагины сторонних разработчиков выносятся в отдельные файлы и валидировать их не нужно.
- Привязывать обработчики js-событий нужно к отдельным классам, не связанным со стилевым оформлением элементов страницы. Это правило актуально, если привязка обработчиков идет к классам.

### Требования к файлам
Векторные и растровые графические файлы должны быть оптимизированы. У растровых графических файлов оптимизация производится путем сжатия изображений, уменьшения разрешения и качества в разумных пределах. Уменьшение изображения на странице через CSS стили не разрешается. У векторных графических файлов – путем удаления ненужных, «мусорных» тегов и правильной группировки свойств, а также минификации кода.

Просим внимательно отнестись к данному пункту. Особенно это относится к оформительской графике. Неоправданно "тяжелые" растровые файлы, либо векторная графика с кучей «мусорных» тегов, полученная после экспорта ее из графического редактора, приниматься не будут!

Файлы шрифтов должны быть представлены в 3х форматах каждый - woff2, woff, ttf. Шрифты не должны подключаться из сторонних источников, а должны лежать в папке fonts;

> Если шрифты подключаются с https://fonts.google.com требование выше не действительно.

Везде, где представляется возможным, использовать векторные изображения. Использование SVG или шрифтовых иконок имеет одинаковый приоритет. Использование обоих методов в рамках одного проекта не возбраняется. Для растровых оформительских изображений и иконок предусмотреть вариант для Retina-мониторов (@2x).

Внимание, важно! Если в конечном варианте верстки файлы стилей и скриптов минифицированные, то помимо них **ОБЯЗАТЕЛЬНО** должны быть предоставлены либо **исходные файлы** (.pug, .jade, .scss  и другие – в зависимости от используемого шаблонизатора / препроцессора), либо неминифицированные отдельные  файлы используемых js-скриптов и стилей. 

# Правила наименования папок и компонентов
- Название папок пишем с маленькой буквы, а название компонентов с большой.
- В папке `components` разделяем компоненты на сущности и помещаем их в соответствующую папку.
- Дополнительные папки в `components`: ui,ui->forms, layout, layout->header,layout->footer, modal, ...

## Структура папок

- `assets` — Исходные файлы scss, fonts, sounds.
- `components` — Компоненты приложения.
- `layouts` — Шаблоны, например страница авторизации
- `middleware` — Позволяет определять пользовательские функции, которые могут быть запущены до отображения страницы или группы страниц (layouts).
- `pages` — Представления (наши страницы) и по тому как они расположены в этой папке он строит маршруты
- `plugins` — Плагины
- `static` — Статические файлы, например robots.txt
- `store` — Хранилище Vuex
- [Полное описание структуры папок](https://ru.nuxtjs.org/docs/2.x/directory-structure/nuxt)

# Полезные ссылки
- [NuxtJS](https://ru.nuxtjs.org/)
- [Vuex](https://vuex.vuejs.org/ru/)
- [nuxt/auth](https://auth.nuxtjs.org/)
- [vee-validate](https://vee-validate.logaretm.com/v4/)
- [Рекомендации Vuejs](https://ru.vuejs.org/v2/style-guide/index.html)  

# Советы
1. Избегайте сложной логики в watch, computed, update или beforeUpdate хуках.
2. Выбирайте между маленькими сторонними библиотеками и полным их отсутствием.
3. Убедитесь, что избавились от ошибок и предупреждений в консоле
4. Показывайте состояния загрузки и ошибок в ваших асинхронных компонентах
5. Используйте `try catch` при запросе к серверу
6. Не мутируйте пропсы. Если пропс нужно изменять в компоненте клонируйте его
7. Не используйте yarn на продакшене

# Тестирование

## Требование к отображению
_**Сайт должен корректно и единообразно отображаться во всех основных браузерах**_

**Десктопные браузеры**
- Microsoft Edge 82.0 и выше (MS Windows);
- Safari 13 и выше (MacOS).

**Мобильные браузеры**
- Microsoft Edge 82.0 и выше (MS Windows);
- Safari 13 и выше (MacOS).
- Mozilla Firefox (MS Windows, Linux, MacOS); _поддержка двух последних версий_
- Opera (MS Windows, Linux, MacOS); _поддержка двух последних версий_
- Google Chrome (MS Windows, Linux, MacOS); _поддержка двух последних версий_
- Yandex Browser (MS Windows, MacOS). _поддержка двух последних версий_

## Требования к поведению верстки
- Верстка должна быть адаптивной т.е. подстраиваться под размер экрана устройств, с которых она просматривается. Макеты всех необходимых разрешений будут предоставлены дизайнером;
- В стилях обязательно должна быть задана минимальная ширина макета - 320 пикселей. При ширине выше этого значения не должно появляться горизонтальной полосы прокрутки страницы;
- В качестве основы для верстки приветствуется (но не является обязательным) использование фреймворка (Bootstrap, Foundation);
- На мобильных устройствах в верстке необходимо отключить автоматическое определение телефонных номеров, так как оно не только портит визуальное отображение, но и может негативно сказаться на работоспособности функционала;
- Оформительская графика не должна размываться на мониторах высокой четкости (Retina-ready верстка);
- Должна корректно отображаться версия для печати (media print), про которую часто забывают. Типографика должна быть приведена в пригодный для печати вид, лишние блоки, фоны и картинки должны быть скрыты;
- Подвал (footer) страницы всегда должен быть прижат к низу страницы;
- Поля форм должны корректно валидироваться при вводе в них значений, либо при потере полем фокуса), если заранее известно их содержание;
- При наведении на изображения и иконки с hover-эффектом не должно быть «мигания». Для этого обычно применяются css-спрайты или data:url;
- Отказоустойчивая верстка. Верстка не должна «ползти» и разваливаться при заполнении страницы контентом, отличным от предложенного в макете. Если в дизайне имеются блоки фиксированной ширины/высоты или в других случаях, когда ввод контента ограничен, предусмотреть обрезание контента путем наложения картинки с градиентом в правый нижний угол, либо иным методом, предусмотренным в макетах дизайна.
- На мобильных устройствах предусмотреть "блокировку поворота экрана" там, где это необходимо, исходя из дизайн-макетов.

Пример - при повороте экрана в ландшафтную ориентацию должна появляться «заглушка» с затемнением, чтобы пользователь вернул экран в вертикальное положение.

## Браузерные расширения
- [Vue.js devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)
- [PerfectPixel](https://chrome.google.com/webstore/detail/perfectpixel-by-welldonec/dkaagdgjmgdmbnecmcefdhjekcoceebi?hl=ru)

> Каждый вышеперечисленный пункт может быть переопределен на уровне проекта или даже страницы. Если вы не согласны с каким-то из пунктов - обсудите это с тимлидом.
