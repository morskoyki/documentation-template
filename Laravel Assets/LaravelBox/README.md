<p align="center"><a href="https://appfox.ru" target="_blank"><img src="https://wiki.appfox.ru/uploads/images/system/2022-08/unknown.png" width="48" alt=""><span style="display: block; font-size: 25px; color: black; font-weight: 700"><span style="color:#ee5735">APP</span>FOX</span></a></p>

[Документация Postman](https://documenter.getpostman.com/view/16912273/2s8Yt1qp5K)

## Настройка серверного окружения 
> Документация написана для настройки сервера на ОС Ubuntu 22.04

Первым делом прописываем команду
`sudo apt update`
### Nginx
`sudo apt install nginx`

### MySQL
`sudo apt install mysql-server`

`sudo mysql_secure_installation`

Придумываем Root пароль для MySQL, обязательно передаем ПМу для сохранности

`sudo mysql`

mysql > `CREATE DATABASE example_database;`

### PHP 8.1
`sudo apt install php8.1 php8.1-mysql php8.1-common php8.1-dom php8.1-bcmath openssl php8.1-mbstring`

### Настройка nginx для вашего проекта
`mkdir /var/www/project_name`


`sudo nano /etc/nginx/sites-available/project_name`

#### Конфигурационный файл для Laravel
```
server {
    listen 80;
    server_name project_domain.ru;
    root /var/www/project_name;

    index index.php index.html index.htm;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
     }

    location ~ /\.ht {
        deny all;
    }

}
```

### Конфигурационный файл для Vue

```

```

`sudo ln -s /etc/nginx/sites-available/project_name /etc/nginx/sites-enabled/`

`sudo unlink /etc/nginx/sites-enabled/default`

`service nginx restart`

Далее, если есть необходимость, ставим бесплатный SSL сертификат

`sudo apt install certbot python3-certbot-nginx`

`certbot --nginx`

Выбираем наш домен ( который указан в конфиге nginx ) и если требуется редирект с http на https выбираем нужный нам пункт
