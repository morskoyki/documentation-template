<?php

namespace App\Traits;

use App\Http\Requests\LoginByEmailPasswordRequest;
use App\Http\Requests\RegisterByEmailPasswordRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

trait AuthByEmailPassword
{
    protected string $authField = 'email';

    public function login(LoginByEmailPasswordRequest $request)
    {
        $user = User::query()->where($this->authField, $request->get($this->authField))->first();

        if(!$user || !Hash::check($request->get('password'), $user->password)) {
            return response()->json([
                'message' => "Invalid {$this->authField} or password"
            ], 401);
        }

        $user->tokens()->delete();
        $user->api_token = $user->createToken($user->phone, ['role:user'])->plainTextToken;

        return UserResource::make($user);
    }

    public function register(RegisterByEmailPasswordRequest $request)
    {
        $hasExistCredentials = User::query()->where($this->authField, $request->get($this->authField))->exists();
        if($hasExistCredentials) {
            return response()->json([
                'message' => "Credentials already exists"
            ], 401);
        }

        $user = User::query()->create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'password' => Hash::make($request->get('password'))
        ]);

        $user->api_token = $user->createToken($user->{$this->authField}, ['role:user'])->plainTextToken;

        return UserResource::make($user);
    }
}
