<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use JetBrains\PhpStorm\ArrayShape;

trait BaseRestTrait
{
    public function getResponse(mixed $data, int $httpStatus = 200): JsonResponse
    {
        return Response::json($data, $httpStatus);
    }

    public function getErrorResponse($message, int $httpStatus = 400, array $errors = []): JsonResponse
    {
        return $this->getResponse($this->answerGenerate($message, $errors), $httpStatus);
    }

    public function getModelAlreadyExists($message, int $httpStatus = 409, array $errors = []): JsonResponse
    {
        return $this->getResponse($this->answerGenerate($message, $errors), $httpStatus);
    }

    public function getModelNotFound($message, int $httpStatus = 404, array $errors = []): JsonResponse
    {
        return $this->getResponse($this->answerGenerate($message, $errors), $httpStatus);
    }

    public function getSuccessResponse(int $httpStatus = 204): JsonResponse
    {
        return $this->getResponse(['message' => 'success'], $httpStatus);
    }

    #[ArrayShape(["message" => "", "errors" => "array[]|mixed"])]
    public function answerGenerate($message, $errors): array
    {
        if(empty($errors)){
            $errors = ['error' => [$message]];
        }
        return [
            "message" => $message,
            "errors"  => $errors
        ];
    }

    public function validationID($id, $tableName): void
    {
        $valid = 'exists:' . $tableName . ',id';
        Validator::make(['id' => $id], [
            'id' => ['integer', $valid]
        ])->validate();
    }

    public function getAnswerResponse($response, $key = null): JsonResponse
    {
        if(data_get($response, 'success')){
            if(empty($key)) {
                return $this->getSuccessResponse();
            }

            return $this->getResponse(data_get($response, $key));
        }

        return $this->getErrorResponse(data_get($response, 'message'));
    }
}
