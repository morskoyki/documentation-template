<?php

namespace App\Http\Controllers\Api;

use App\Services\UserService;
use Illuminate\Http\JsonResponse;

class UserController extends BaseApiController
{
    protected UserService $service;

    public function __construct()
    {
        parent::__construct();
        $this->service = new UserService($this->user);
    }

    public function me(): JsonResponse
    {
        return $this->getResponse(
            $this->service->me()
        );
    }
}
