<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\LoginByEmailPasswordRequest;
use App\Http\Requests\LoginByPhoneCodeRequest;
use App\Http\Requests\RegisterByEmailPasswordRequest;
use App\Http\Requests\RegisterByPhoneCodeRequest;
use App\Services\AuthService;
use Illuminate\Http\JsonResponse;

class AuthController extends BaseApiController
{
    protected AuthService $service;

    public function __construct()
    {
        parent::__construct();
        $this->service = new AuthService();
    }

    public function login(LoginByPhoneCodeRequest $request): JsonResponse
    {
        return $this->getResponse(
            $this->service->login($request)
        );
    }

    public function register(RegisterByPhoneCodeRequest $request): JsonResponse
    {
        return $this->getResponse(
            $this->service->register($request)
        );
    }

    public function logout(): JsonResponse
    {
        $this->service->logout($this->user);

        return $this->getSuccessResponse();
    }
}
