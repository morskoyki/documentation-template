#### Реализация игрового процесса улучшения (прокачки) сущности на примере проекта "1901"

Серверная часть проекта написана на PHP-фреймворке [Laravel](https://laravel.com/)

**Задача:**

Часто в играх присутствует множество сущностей, которые можно улучшать (прокачивать). Например, здания, юниты.
Этот процесс, как правило, не моментальный, а идет какое-то время, поэтому необходимо постоянно мониторить окончание
этого события, чтобы зафиксировать факт прокачки.

**Реализация:**

Рассмотрим это на примере улучшения здания до следующего уровня

- в базе данных создаем таблицу builds, где помимо прочих будут поле _end_of_upgrade_ с типом _DATETIME_

```
$table->dateTime('end_of_upgrade');
```

- при старте улучшения здания будем записывать в это поле дату и время окончания этого процесса
- для мониторинга процесса создадим консольную команду, которая по истечении времени процесса
  будет повышать уровень здания

  _Можно, конечно, создать для этих целей `Job`, но тогда минимальный период проверки будет 1 раз в минуту,
  что не всегда подходит, так как сам процесс может идти, например, 40 секунд, а до следующей проверки
  осталось, например, 20 секунд и получается, что время улучшения составит 60 секунд._

```php
<?php

namespace App\Console\Commands;

use App\Models\Build;
use Illuminate\Console\Command;

class UpgradeBuildCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'endUpgradeBuild:check';

    /**
     * The console command description.
     *
     * @var string
     */ 
    protected $description = 'Command description';

    public function handle()
    {
        while (true) {
            // получаем здания, у которых процесс улучшения завершен
            $builds = Build::where('end_of_upgrade', '<', now())->get();

            // у каждого из зданий увеличиваем уровень и обнуляем поле 'end_of_upgrade'
            foreach ($builds as $item) {

                $item->increment(
                    'level',
                    1,
                    [
                        'end_of_upgrade' => null,
                    ]
                );
            }   
        }
    }
}
```

Для тестирования этого процесса во время локальной разработки можно использовать консольную команду

`php artisan endUpgradeBuild:check`

На production сервере для запуска команды будем использовать [Supervisor](http://supervisord.org/) с такой конфигурацией

```
[program:build-upgrade-checking]
process_name=%(program_name)s_%(process_num)02d
command=php artisan endUpgradeBuild:check
directory=/var/www/html
stdout_logfile=/var/www/html/storage/logs/build-upgrade-checking.log
redirect_stderr=true
autostart=true
autorestart=true
```
