import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:math' as math;

class AppFoxCenter extends StatefulWidget {
  const AppFoxCenter({Key? key}) : super(key: key);

  @override
  State<AppFoxCenter> createState() => _AppFoxCenterState();
}

class _AppFoxCenterState extends State<AppFoxCenter> with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late double size;

  @override
  void initState() {
    _controller = AnimationController(vsync: this, duration: const Duration(seconds: 4))..repeat();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    var query = MediaQuery.of(context);
    size = query.orientation == Orientation.portrait ? query.size.height : query.size.width;
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: size / 5,
        height: size / 5,
        child: Stack(
          fit: StackFit.expand,
          children: [
            SvgPicture.asset(
              'packages/appfox_splash/lib/src/assets/appfox_main.svg',
            ),
            Positioned(
              top: size / 13.5,
              left: size / 14.5,
              child: AnimatedBuilder(
                animation: _controller,
                builder: (_, child) {
                  return Transform.rotate(
                    angle: _controller.value * 2 * math.pi,
                    child: child,
                  );
                },
                child: SizedBox(
                  height: size / 72,
                  width: size / 72,
                  child: CustomPaint(painter: _EyePainter()),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _EyePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.black
      ..strokeWidth = 4
      ..strokeCap = StrokeCap.round;

    Offset start1 = const Offset(0, 0);
    Offset end1 = Offset(size.width, size.height);
    Offset start2 = Offset(size.width, 0);
    Offset end2 = Offset(0, size.height);

    canvas.drawLine(start1, end1, paint);
    canvas.drawLine(start2, end2, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
